<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'type' => 'Pierwszy',
            ],
            [
                'type' => 'Drugi',
            ],
            [
                'type' => 'Trzeci',
            ]
        ];

        foreach ($types as $type)
            DB::table('class_types')->insert($type);
    }
}
